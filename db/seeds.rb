# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Spree::Core::Engine.load_seed if defined?(Spree::Core)
Spree::Auth::Engine.load_seed if defined?(Spree::Auth)

electronic = Spree::ShippingCategory.find_or_create_by!(:name => "Electronic")
admission = Spree::TaxCategory.create!(:name => "Admission Charge")

default_attrs = {
  available_on: Time.zone.now,
  tax_category: admission,
  shipping_category: electronic
}

products = [
  {
    name: "Full Course",
    description: "The Whole Enchilada",
    price: 699.00
  },
  {
    name: "Intro Package",
    description: "Tuesday & Wednesday. This package covers in-depth the logic and context of violence, conflict communication, and a practical overview of physical self-defense.",
    price: 249.00
  },
  {
    name: "Advanced Package",
    description: "Three days. Friday – Sunday. This is new material; weapons familiarization, environmental training, threat assessment, urban stalking.",
    price: 349.00
  },
  {
    name: "Weekend Only",
    description: "A shorter version of the advanced package.",
    price: 299.00
  },
  {
    name: "Individual Days",
    description: "Please specify the days for which you would like to register as a note to the seller when you make your payment.",
    price: 149.00
  }
]

Spree::Config[:currency] = "USD"
products = products.map do |product_attrs|
  Spree::Product.create!(default_attrs.merge(product_attrs))
end

day = Spree::OptionType.create!({
  :name => "day",
  :presentation => "Day of the Week",
  :position => 1
})

days = Spree::OptionValue.create!([
  {
    :name => "Tuesday",
    :presentation => "Tue",
    :position => 2,
    :option_type => day
  },
  {
    :name => "Wednesday",
    :presentation => "Wed",
    :position => 3,
    :option_type => day
  },
  {
    :name => "Thursday",
    :presentation => "Thu",
    :position => 4,
    :option_type => day
  },
  {
    :name => "Friday",
    :presentation => "Fri",
    :position => 5,
    :option_type => day
  },
  {
    :name => "Saturday",
    :presentation => "Sat",
    :position => 6,
    :option_type => day
  },
  {
    :name => "Sunday",
    :presentation => "Sun",
    :position => 7,
    :option_type => day
  }
])

full, intro, advanced, weekend, individual = products
variants = [
  {
    :product => individual,
    :option_values => days,
    :sku => "individual-00001",
    :cost_price => 149
  }
]

Spree::Variant.create!(variants)